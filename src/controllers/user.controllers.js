const User = require('../models/user.model.js');
var crypto = require('crypto');
var mongoose = require('mongoose') // import it 
ENCRYPTION_KEY='my_32_chars_encryption_key_12345';
const IV_LENGTH = 16;

const jwt = require('jsonwebtoken');
jwtKey = "jwt";

//Register user
exports.register = (req, res) => {
    let iv = crypto.randomBytes(IV_LENGTH);
	let cipher = crypto.createCipheriv('aes-256-cbc', new Buffer.from(ENCRYPTION_KEY), iv);
	let encrypted = cipher.update(req.body.password);
    encrypted = Buffer.concat([encrypted, cipher.final()]);
	pass = iv.toString('hex') + ':' + encrypted.toString('hex');
    // console.log(pass);
    const data = new User({
        _id:mongoose.Types.ObjectId(),
        name:req.body.name,
        email:req.body.email,
        password:pass,
    })
    data.save().then((result) => {
        jwt.sign({result}, jwtKey, {expiresIn:'2d', issuer: 'https://scotch.io'}, (err, token) => {
            console.log(result)
            res.status(201).json(result);
        })
    })
    .catch((error) => console.error(error))
};

// User Login
exports.login = (req, res) => {
    console.log('here')
    User.findOne({email:req.body.email}).then((data) => {

        console.log('here1111')
        console.warn(data)
        let textParts = data.password.split(':');
        let iv = new Buffer.from(textParts.shift(), 'hex');
        let encryptedText = new Buffer.from(textParts.join(':'), 'hex');
        let decipher = crypto.createDecipheriv('aes-256-cbc', new Buffer.from(ENCRYPTION_KEY), iv);
        let decrypted = decipher.update(encryptedText);

        decrypted = Buffer.concat([decrypted, decipher.final()]);
        pass = decrypted.toString();
        console.log('2222')
        if(pass == req.body.password){
            console.log('3333')
            jwt.sign({data}, jwtKey, {expiresIn:'2d', issuer: 'https://scotch.io'}, (err, token) => {
                res.status(200).json({token,data});
            })
            console.log('4444')
        }

    })
};

// Get and return users from the database.
exports.findAll = (req, res) => {
    console.log("Controller After MiddleWare");
    User.find()
    .then(users => {
        res.send(users);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Something went wrong."
        });
    });
};

// Create and Save a new User
exports.create = (req, res) => {
    // Validate request
    if(!req.body) {
        return res.status(400).send({
            message: "Please fill all required field"
        });
    }

    // Create a new User
    const user = new User({
        name: req.body.name, 
        email: req.body.email,
        password: req.body.password
    });

    // Save user in the database
    user.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Something went wrong."
        });
    });
};

// Get a single User with a id
exports.findOne = (req, res) => {
    User.findById(req.params.id)
    .then(user => {
        if(!user) {
            return res.status(404).send({
                message: "User not found with id " + req.params.id
            });            
        }
        res.send(user);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "User not found with id " + req.params.id
            });                
        }
        return res.status(500).send({
            message: "Error getting user with id " + req.params.id
        });
    });
};

// Update a User identified by the id in the request
exports.update = (req, res) => {
    // Validate Request
    if(!req.body) {
        return res.status(400).send({
            message: "Please fill all required field"
        });
    }

    // Find user and update it with the request body
    User.findByIdAndUpdate(req.params.id, {
        name: req.body.name, 
        email: req.body.email,

    }, {new: true})
    .then(user => {
        if(!user) {
            return res.status(404).send({
                message: "user not found with id " + req.params.id
            });
        }
        res.send(user);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "user not found with id " + req.params.id
            });                
        }
        return res.status(500).send({
            message: "Error updating user with id " + req.params.id
        });
    });
};

// Delete a User with the specified id in the request
exports.delete = (req, res) => {
    User.findByIdAndRemove(req.params.id)
    .then(user => {
        if(!user) {
            return res.status(404).send({
                message: "user not found with id " + req.params.id
            });
        }
        res.send({message: "user deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "user not found with id " + req.params.id
            });                
        }
        return res.status(500).send({
            message: "Could not delete user with id " + req.params.id
        });
    });
};