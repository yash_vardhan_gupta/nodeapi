const express = require('express')
const router = express.Router()
const userController = require('../controllers/user.controllers');
const middlewareA = require('../middleware/middlewareA.js');


// Register new user
router.post('/register', userController.register);
// Login a user
router.post('/login', userController.login);

// Get all users
router.get('/',middlewareA, userController.findAll);

// Get all users
// router.get('/', userController.findAll);

// Create a new user
router.post('/', userController.create);

// Get a single user with id
router.get('/:id', userController.findOne);

// Update a user with id
router.put('/:id', userController.update);

// Delete a user with id
router.delete('/:id', userController.delete);

module.exports = router